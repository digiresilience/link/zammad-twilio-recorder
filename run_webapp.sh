#! /usr/bin/env bash

set -e

if [ -f .env ]; then
  echo "sourcing local env"
  source .env
fi

workers=${2:-4}

RUN_COMMAND="talisker.gunicorn recorder.record:app --bind $1 --worker-class sync --workers ${workers} --name talisker-`hostname`"

if [ "${FLASK_DEBUG}" = true ] || [ "${FLASK_DEBUG}" = 1 ]; then
    RUN_COMMAND="${RUN_COMMAND} --reload --log-level debug --timeout 9999"
fi

${RUN_COMMAND}
