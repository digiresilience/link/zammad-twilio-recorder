#! /usr/bin/env bash

set -e

if [ "$1" = 'webapp' ]; then
  bind_arg=${2:-0.0.0.0:8000}
  ./run_webapp.sh "$bind_arg"
fi
if [ "$1" = 'processor' ]; then
  ./run_huey.sh
fi
