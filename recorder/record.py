import os
import base64
import urllib3
from functools import wraps
from flask import abort, request, make_response, send_file
from talisker.flask import TaliskerApp
from logging import getLogger
from werkzeug.contrib.fixers import ProxyFix
from werkzeug.debug import DebuggedApplication
from huey import RedisHuey
from twilio.twiml.voice_response import VoiceResponse
from twilio.request_validator import RequestValidator
from twilio.rest import Client
from zammad_py import ZammadAPI

from recorder.util import validate_cfg, get_cfg, is_devel_mode

validate_cfg(os.environ.get("RECORDER_MODE", "webapp"))
app = TaliskerApp(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
logger = getLogger(__name__)
if app.debug:
    app.wsgi_app = DebuggedApplication(app.wsgi_app)

huey_db = get_cfg("REDIS_URL")
huey = RedisHuey(url=huey_db)


def validate_twilio_request(f):
    """Validates that incoming requests genuinely originated from Twilio"""

    @wraps(f)
    def decorated_function(*args, **kwargs):
        # Create an instance of the RequestValidator class
        t = get_cfg("TWILIO_AUTH_TOKEN")
        validator = RequestValidator(t)

        # Validate the request using its URL, POST data,
        # and X-TWILIO-SIGNATURE header
        request_valid = validator.validate(
            request.url, request.form, request.headers.get("X-TWILIO-SIGNATURE", "")
        )

        # Continue processing the request if it's valid, return a 403 error if
        # it's not
        if request_valid or is_devel_mode():
            return f(*args, **kwargs)
        else:
            return abort(403)

    return decorated_function


@app.route("/", methods=["GET"])
def root():
    return make_response("twilio recorder ready", 200)


@app.route("/prompt", methods=["GET"])
@validate_twilio_request
def prompt():
    """
    Returns the MP3 file to play as a prompt to the caller
    """
    audio_path = get_cfg("RECORDING_PROMPT_MP3_PATH")
    with open(audio_path, "r") as f:
        try:
            return send_file(audio_path, mimetype="audio/mpeg")
        except Exception as e:
            logger.error("error sending prompt audio")
            logger.error(e)
            return make_response("ERROR", 500)


@app.route("/record", methods=["GET"])
def record_get():
    return make_response("twilio recorder ready", 200)


@app.route("/record", methods=["POST"])
@validate_twilio_request
def record():
    """Returns TwiML which prompts the caller to record a message"""
    response = VoiceResponse()

    audio = get_cfg("RECORDING_PROMPT_MP3_PATH")
    if audio is None:
        response.say(
            get_cfg("RECORDING_PROMPT", "Hello. Please leave a message after the beep.")
        )
    else:
        base_url = get_cfg("RECORDER_BASE_URL")
        audio_url = "{}/prompt".format(base_url)
        response.play(url=audio_url)

    response.record(
        play_beep="true",
        finish_on_key="1",
        recording_status_callback="/twilio_recording",
    )

    response.hangup()

    logger.debug("twiml response for inbound call", extra={"twiml": str(response)})
    return str(response)


@app.route("/twilio_recording", methods=["GET", "POST"])
@validate_twilio_request
def twilio_recording():
    """
    Listen for recording status from Twilio, when received
        parse the recording URL,
        download the recording,
        send it off to be transcribed and
        then send the transcription back via SMS
    """
    recording_url = request.values.get("RecordingUrl")
    call_sid = request.values["CallSid"]
    duration = int(request.values["RecordingDuration"])
    status, recording = get_twilio_recording(recording_url)
    if status == 200:
        if duration >= get_cfg("MIN_CALL_LENGTH", 3):
            res = process_call(call_sid, recording, "audio/mpeg")
            logger.info("call queued for processing", extra={"call_sid": call_sid})
        else:
            logger.info(
                "skipping call due to short duration",
                extra={"call_sid": call_sid, "duration_secs": duration},
            )
        return make_response("OK", 203)
    else:
        logger.error(
            "error downloading recording for call",
            extra={"call_sid": call_sid, "recording_url": recording_url},
        )
        return make_response("ERROR", 500)


def get_twilio_recording(recording_url):
    """
    Retrieve the Recording from Twilo and return the actual data
    """

    twilio_account_sid = get_cfg("TWILIO_ACCOUNT_SID")
    twilio_auth_token = get_cfg("TWILIO_AUTH_TOKEN")
    twilio_creadentials = twilio_account_sid + ":" + twilio_auth_token
    twilio_headers = urllib3.make_headers(basic_auth=twilio_creadentials)
    http = urllib3.PoolManager()
    recording = http.request("GET", recording_url + ".mp3", headers=twilio_headers)
    return recording.status, recording.data


def get_customer(client, phone_number):
    phone_number = phone_number.replace("+", "")
    results = client.user.search({"query": f"phone:{phone_number}"})
    if len(results) > 1:
        return results[0]
    return None


def get_or_create_customer(client, phone_number):
    c = get_customer(client, phone_number)
    if c is not None:
        return c
    c = client.user.create(
        {"phone": phone_number, "note": "Customer created from incoming voicemail"}
    )
    return c


def create_ticket(call, recording_data_base64, mimetype):
    token = get_cfg("ZAMMAD_TOKEN")
    host = get_cfg("ZAMMAD_HOST")
    group = get_cfg("ZAMMAD_GROUP")
    client = ZammadAPI(http_token=token, host=host, is_secure=True)

    subject = "Call from {} at {}".format(call.from_formatted, call.start_time)
    body = """
    <ul>
    <li>Caller: {from_formatted}</li>
    <li>Service Number: {to_formatted}</li>
    <li>Call Duration: {duration} seconds</li>
    <li>Start Time: {start_time}</li>
    <li>End Time: {end_time}</li>
    </ul>
    <p>See the attached recording.</p>
    """.format(
        **{
            "from_formatted": call.from_formatted,
            "to_formatted": call.to_formatted,
            "duration": call.duration,
            "start_time": call.start_time,
            "end_time": call.end_time,
        }
    )

    customer = get_or_create_customer(client, call.from_formatted)
    customer_id = customer["id"]

    filename = "{}{}.mp3".format(call.start_time, call.from_)
    payload = {
        "title": subject,
        "group": group,
        "note": "This ticket was created from a recorded voicemail.",
        "customer_id": customer_id,
        "article": {
            "subject": subject,
            "body": body,
            "content_type": "text/html",
            "type": "note",
            "attachments": [
                {
                    "filename": filename,
                    "data": recording_data_base64,
                    "mime-type": mimetype,
                }
            ],
        },
    }

    res = client.ticket.create(payload)
    if res["id"] is not None:
        logger.info("created ticket", extra={"ticket_id": res["id"]})
        return res["id"]
    else:
        logger.error("failed to create zammad ticket", extra={"result": res})
        raise Exception("failed to create zammad ticket")


@huey.task(retries=100, retry_delay=60)
def process_call(call_sid, recording_data, mimetype):
    twilio_account_sid = get_cfg("TWILIO_ACCOUNT_SID")
    twilio_auth_token = get_cfg("TWILIO_AUTH_TOKEN")

    client = Client(twilio_account_sid, twilio_auth_token)
    call = client.calls(call_sid).fetch()

    recording_data_base64 = base64.b64encode(recording_data).decode("utf-8")
    create_ticket(call, recording_data_base64, mimetype)


def main():
    debug = os.getenv("DEBUG", "true").lower() == "true" or is_devel_mode()
    app.run(debug=debug, host="127.0.0.1", port=5000)


if __name__ == "__main__":
    main()
