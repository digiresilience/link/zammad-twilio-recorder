import os


def check_rw_perms(path):
    if os.path.exists(path):
        if os.path.isfile(path):
            return os.access(path, os.W_OK)
        else:
            return False
    parent = os.path.dirname(path)
    return os.access(parent, os.W_OK)


def get_cfg(key, default=None):
    """Returns a value from the environment, first trying the passed key
    then trying the _FILE variant, reading the value from the file if it exists
    """
    try:
        return os.environ[key]
    except KeyError:
        try:
            with open(os.environ[key + "_FILE"], "r") as f:
                return f.read().strip()
        except (KeyError, OSError):
            return default


class ConfigurationError(Exception):
    pass


def validate_cfg(mode="webapp"):
    required_keys = [
        "TWILIO_AUTH_TOKEN",
        "TWILIO_ACCOUNT_SID",
        "ZAMMAD_HOST",
        "ZAMMAD_TOKEN",
        "ZAMMAD_GROUP",
        "REDIS_URL",
        #'RECORDING_PROMPT',# this one is not required
        #'RECORDING_PROMPT_MP3_PATH', # this one is not required
        #'MIN_CALL_LENGTH' # not required
        "RECORDER_BASE_URL",
    ]
    missing = []
    for k in required_keys:
        v = get_cfg(k)
        if v is None:
            missing.append(k)

    if len(missing) > 0:
        raise ConfigurationError(
            "Required config values not found: {}".format(", ".join(missing))
        )
    if mode == "webapp":
        base_url = get_cfg("RECORDER_BASE_URL")
        if not base_url.startswith("http"):
            raise ConfigurationError(
                'RECORDER_BASE_URL value "{}" invalid. Must be a valid url like "https://example.com"'.format(
                    base_url
                )
            )

        audio_path = get_cfg("RECORDING_PROMPT_MP3_PATH")
        if audio_path is not None and not os.access(audio_path, os.R_OK):
            raise ConfigurationError(
                "RECORDING_PROMPT_MP3_PATH file is not readable or doesn't exist, file={}".format(
                    audio_path
                )
            )


def is_devel_mode():
    v = get_cfg("DEVEL", False)
    return v is True or v == "true" or v == "1"
