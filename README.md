# zammad-twilio-recorder

A service that accepts twilio Record requests and creates a ticket in Zammad.

The service consists of a small Flask application used as a twilio webhook. It
receives calls from a number, records them, and then uploads the recorder to
Zammad as a ticket.

The call processing and uploading to Zammad happens in a background job using
[huey](https://huey.readthedocs.io) as a persistent queue to ensure
reliability.

## Development

1. Create your python3 virtualenv `python3 -m virtualenv venv`, `source ./venv/bin/activate`, then `pip install -r requirements.txt`
2. Run a local redis instance
3. You need to have the following env vars set:

  ```
  TWILIO_AUTH_TOKEN=xxx
  TWILIO_ACCOUNT_SID=yyyy
  ZAMMAD_TOKEN: xxx
  ZAMMAD_HOST: zammad.example.org
  ZAMMAD_GROUP: Users
  FLASK_DEBUG: 1 # or 0 for prod mode
  REDIS_URL: redis://localhost:6379
  RECORDER_BASE_URL: https://twilio-recorder.example.org
  RECORDING_PROMPT: "Please leave a message after the beep"
  RECORDING_PROMPT_MP3_PATH: "/path/to/prompt.mp3"
  MIN_CALL_LENGTH: 5
  ```

4. Run the web server `./run_webapp.sh`
5. Run the background job processor `./run_huey.sh`


The ZAMAMD_TOKEN should be a token from a zammad user. The token needs the agent.ticket permission.

## Production

Docker images are available at
[digiresilience/zammad-twilio-recorder](https://hub.docker.com/r/digiresilience/zammad-twilio-recorder) on Docker Hub.

Images are signed with Docker Content Trust (DCT). The DCT root key id is
`23a580d0ed226ad117d5e2f41e7d373e00c90faa54b43ca9964b37aae7b22e93`.

For deployment you will need to run two instances of the container, one for the
webapp and one for the call processing. `docker-compose.yml` has a minimal
deployment setup.
