FROM python:3.6-stretch
LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="abeluck/zammad-twilio-recorder"
LABEL org.label-schema.description="Record Twilio calls and make them Zammad Tickets"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION

ENV LANG C.UTF-8
EXPOSE 8000
ARG WORK_DIR=/opt/zammad-twilio
ARG RUN_DIR=/var/lib/zammad-twilio-recorder
RUN groupadd -r app && useradd -r -g app -d ${RUN_DIR} -s /sbin/nologin -c "Docker image user" app
RUN mkdir -p ${WORK_DIR}/
RUN mkdir -p /srv/prompt && chmod 555 /srv/prompt
RUN mkdir -p ${RUN_DIR}/ && chown -R app:app ${RUN_DIR}
VOLUME ${RUN_DIR}
WORKDIR ${WORK_DIR}/
ADD ./requirements.txt ./requirements.txt
RUN pip install -r requirements.txt
COPY . ${WORK_DIR}/
RUN python setup.py install
USER app
ENTRYPOINT ["./docker_entrypoint.sh"]
