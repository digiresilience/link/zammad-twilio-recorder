"""Installer for zammad-twilio-recorder
"""

import os
from setuptools import setup, find_packages


setup(
    name="zammad-twilio-recorder",
    description="A service that accepts twilio Record requests and creates a ticket in Zammad",
    long_description=open("README.md").read(),
    version="0.0.2",
    author="Abel Luck",
    author_email="abel@guardianproject.info",
    url="https://gitlab.com/digiresilience/link/zammad-twilio-recorder",
    packages=find_packages(exclude=["ez_setup"]),
    install_requires=open(
        os.path.join(os.path.dirname(__file__), "requirements.txt")
    ).readlines(),
    entry_points={"console_scripts": ["zammad-twilio-recorder = recorder.record:main"]},
    license="AGPL3",
)
