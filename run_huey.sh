#!/bin/bash

if [ -f .env ]; then
  echo "sourcing local env"
  source .env
fi

# Run the consumer with 2 worker threads.
huey_consumer.py recorder.processor.huey -w2
