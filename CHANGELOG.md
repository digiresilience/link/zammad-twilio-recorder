# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a
Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] - 2019-06-25

### Added

* Added `RECORDER_BASE_URL` as a required env variable containing the base url
  to the service's endpoint.
* The text-to-speech voice prompt can be customized via `RECORDING_PROMPT`
* Instead of text-to-speech prompt you can provide an MP3 file to play as the
  prompt, configurable with `RECORDING_PROMPT_MP3_PATH`

### Changed

* Recordings shorter than 5 seconds by default are not processed (configurable
  with `MIN_CALL_LENGTH`)

### Removed

n/a


## [0.0.1] - 2019-06-24

### Added

* Initial release of zammad-twilio-recorder

[Unreleased]: https://gitlab.com/digiresilience/link/zammad-twilio-recorder/compare/0.0.2...master
[0.0.2]: https://gitlab.com/digiresilience/link/zammad-twilio-recorder/compare/0.0.1...0.0.2
[0.0.1]: https://gitlab.com/digiresilience/link/zammad-twilio-recorder/commits/0.0.1
